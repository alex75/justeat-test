@echo off

rem C:\Windows\Microsoft.NET\Framework64\v4.0.30319\csc.exe  /r:System.IO.Compression.FileSystem.dll "Create Zip.cs"

echo *** This batch create "Create Zip for Just Eat.exe" in the root folder ***
echo. 
echo.
"C:\Program Files (x86)\MSBuild\12.0\Bin\csc.exe" /r:System.IO.Compression.dll /r:System.IO.Compression.FileSystem.dll /out:"../Create Zip for Just Eat.exe"  "Create Zip.cs" 

echo.
echo "Create Zip for Just Eat.exe" created
echo.

pause