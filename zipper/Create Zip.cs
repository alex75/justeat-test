using System;
using System.Linq;
using System.IO;
using System.IO.Compression;

namespace Zipper
{
    public class Program
    {
        static void Main(string[] args)
        {
            string zipFileName = @"Test.zip";
            string zipFileName_temp = zipFileName + "_temp";

            if(File.Exists(zipFileName))
                File.Delete(zipFileName);

            if (File.Exists(zipFileName_temp))
                File.Delete(zipFileName_temp);
            
            ZipFile.CreateFromDirectory("source", zipFileName_temp);
            
            Console.WriteLine("Archive created");
            
            // delete "packeges" folder
            using (ZipArchive zip_temp = ZipFile.Open(zipFileName_temp, System.IO.Compression.ZipArchiveMode.Update))
            using (ZipArchive zip = ZipFile.Open(zipFileName, System.IO.Compression.ZipArchiveMode.Update))
            {
                // remove "packages" 
                foreach (var entry in zip_temp.Entries.Where(e => e.FullName.StartsWith("packages\\")).ToList())
                    entry.Delete();
                Console.WriteLine("\"packages\" removed");

                // move source files in "source" dir
                foreach (var entry in zip_temp.Entries)
                {
                    var newEntry = zip.CreateEntry("source\\" + entry.FullName);

                    int size = 1024 * 4;
                    char[] buffer = new char[size];
                    int length;

                    using (StreamReader reader = new StreamReader(entry.Open()))
                    using (StreamWriter writer = new StreamWriter(newEntry.Open()))
                    {						
                        while ((length = reader.Read(buffer, 0, size)) > 0)
                            writer.Write(buffer, 0, length);
                    }
                    Console.WriteLine(string.Format("\"{0}\" copied", entry.FullName));
                }		

            }

            if (File.Exists(zipFileName_temp))
                File.Delete(zipFileName_temp);

            
            Console.WriteLine("\n\nPress any key to close...");
            Console.ReadKey();            
        }
    }
}