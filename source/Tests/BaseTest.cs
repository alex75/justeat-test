﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace JustEat.Tests
{
	public abstract class TestBase
	{

		protected string apiUrl;

		[TestFixtureSetUp]
		public void BaseTest()
		{
			apiUrl = ConfigurationManager.AppSettings["apiUrl"];	
		}

	}
}
