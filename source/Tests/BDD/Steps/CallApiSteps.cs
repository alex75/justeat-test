﻿using System;
using System.Collections.Generic;
using System.Configuration;
using JustEat.Services;
using JustEat.Services.ApiConsumers;
using JustEat.Services.Builders;
using JustEat.Services.Entities;
using Should.Core.Assertions;
using TechTalk.SpecFlow;

namespace JustEat.Tests.BDD.Steps
{
    [Binding]
    public class CallApiStepsSteps
    {

		string apiUrl;
		private const string outcode = "se19";
		private Exception exception;

		public CallApiStepsSteps()
		{

		}

        [Given(@"I have a wrong API URL into the configuration")]
        public void GivenIHaveAWrongAPIURLIntoTheConfiguration()
        {
			apiUrl = "http://this_is_wrong.just.eat/api/";			
        }        
        
        [When(@"I search the restaurants")]
        public void WhenISearchTheRestaurant()
        {
			IRestaurantListBuilder listBuilder = new RestaurantListBuilder();
			IApiConsumer apiConsumer = new ApiConsumer(apiUrl);
			IRestaurantService service = new RestaurantService(apiConsumer, listBuilder);
			try
			{
				service.ListRestaurants(outcode);
			}
			catch (Exception exc)
			{
				exception = exc;
			}
        }
        
        [Then(@"the result must be an error")]
        public void ThenTheResultMustBeAnError()
        {
			Assert.False(exception == null);			
        }
        
        [Then(@"the text must contains '(.*)'")]
        public void ThenTheTextMustContains(string errorDescription)
        {
			string message = exception.Message;
			Assert.True(message.Contains(errorDescription));
        }
    }
}
