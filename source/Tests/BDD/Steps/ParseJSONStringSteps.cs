﻿using System;
using System.Collections.Generic;
using JustEat.Services.Builders;
using JustEat.Services.Entities;
using TechTalk.SpecFlow;
using Should;

namespace JustEat.Tests.BDD.Steps
{
    [Binding]
    public class ParseJSONStringSteps
    {
		private RestaurantListBuilder builder;
		private IList<Restaurant> restaurants;

		public ParseJSONStringSteps()
		{
			builder = new RestaurantListBuilder();
		}

		private string jsonString;

        [Given(@"I have a JSON string like (.*)")]
        public void GivenIHaveAJSONString(string jsonString)
        {
			//ScenarioContext.Current.Add("jsonString", jsonString);
			this.jsonString = jsonString;
        }
        
        [When(@"I parse it")]
        public void WhenIParseIt()
        {
			restaurants = builder.CreateList(jsonString);
			//ScenarioContext.Current.Add("restaurants", restaurants);
        }
        
        [Then(@"the result is a list of Restaurant")]
        public void ThenTheResultIsAListOfRestaurant()
        {
			restaurants.ShouldNotBeNull();
			restaurants.ShouldNotBeEmpty();
        }

		[Then(@"the first one have a Rating of (.*)")]
		public void ThenTheFirstOneHaveARatingOf(float rating)
		{
			var restaurant = restaurants[0];
			restaurant.Rating.ShouldEqual(rating);
		}


    }
}
