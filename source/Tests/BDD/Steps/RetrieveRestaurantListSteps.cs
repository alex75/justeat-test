﻿using System;
using System.Collections.Generic;
using System.Configuration;
using JustEat.Services;
using JustEat.Services.ApiConsumers;
using JustEat.Services.Builders;
using JustEat.Services.Entities;
using TechTalk.SpecFlow;
using Should;


namespace JustEat.Tests.BDD.Steps
{
    [Binding]
    public class RetrieveRestaurantListSteps
    {

		private readonly IRestaurantService service;
		private string outCode;
		private IList<Restaurant> restaurants;

		public RetrieveRestaurantListSteps()
		{
			string apiUrl = ConfigurationManager.AppSettings["apiUrl"];	
			IApiConsumer apiConsumer = new ApiConsumer(apiUrl);
			IRestaurantListBuilder listBuilder = new RestaurantListBuilder();
			service = new RestaurantService(apiConsumer, listBuilder);
		}

        [Given(@"I have entered the outcode ""(.*)"" in the application")]
        public void GivenIHaveEnteredTheOutcodeInTheApplication(string outCode)
        {
			this.outCode = outCode;
        }
        
        [When(@"I press Enter")]
        public void WhenIPressEnter()
        {
			restaurants = service.ListRestaurants(outCode);
        }
        
        [Then(@"the application shows a list of restaurant")]
        public void ThenTheApplicationShowsAListOfRestaurant()
        {
			restaurants.ShouldNotBeNull();
			restaurants.ShouldNotBeEmpty();
        }
        
        [Then(@"the application shows an empty list of restaurant")]
        public void ThenTheApplicationShowsAnEmptyListOfRestaurant()
        {
			restaurants.ShouldNotBeNull();
			restaurants.ShouldBeEmpty();
        }
    }
}
