﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JustBehave;
using NUnit.Framework;
using Shouldly;

namespace JustEat.Tests.BDD.Behave
{
	public class ShouldExistsOnlyOne :WhenAnAlreadyPresentCousineIsAdded
	{
		[Then, Category("Restaurant")]
		public void CousineTypeIsUnique()
		{
			Restaurant.CuisineTypes.Count(t => t.Name == "Italian").ShouldBe(1);
		}

	}
}
