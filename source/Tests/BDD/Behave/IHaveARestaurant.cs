﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JustBehave;
using JustEat.Services.Entities;

namespace JustEat.Tests.BDD.Behave
{
	/// <summary>
	/// <see cref="https://github.com/justeat/JustBehave"/>
	/// </summary>
	public abstract class IHaveARestaurant : BehaviourTest<Restaurant>
	{

		public Restaurant Restaurant { get; private set; }

		protected override void Given()
		{
			Restaurant = new Restaurant("Restaurant ABC") { };			
		}
	}
}
