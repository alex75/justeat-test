﻿Feature: Parse JSON string
	Retrieved a JSON string from the Web API
	I want to obtain a list of Restaurant

@JSON
Scenario: Parse JSON string
	Given I have a JSON string like {"ShortResultText":"SE19", "Restaurants":[{"Name":"Toscana Italian Pizzeria", "RatingStars":4.98, "CuisineTypes":[{"Name":"Italian"}] } ]}
	When I parse it
	Then the result is a list of Restaurant
	And the first one have a Rating of 4.98

@JSON
Scenario: Parse JSON string without Rating
	Given I have a JSON string like {"ShortResultText":"SE20", "Restaurants":[{"Name":"Toscana Italian Pizzeria", "CuisineTypes":[{"Name":"Italian"}] } ]}
	When I parse it
	Then the result is a list of Restaurant
	And the first one have a Rating of -1
