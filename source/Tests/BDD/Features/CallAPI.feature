﻿Feature: Call API
	In order to have a useful error message
	As a simple user
	I want to be receive an error related to problem about "API URL" when it is wrong

@API
Scenario: Call API with a wrong URL
	Given I have a wrong API URL into the configuration
	When I search the restaurants
	Then the result must be an error
	And the text must contains 'API URL'
