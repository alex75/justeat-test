﻿Feature: Retrieve Restaurant List
	In order to choose a restaurant
	Having an outcode
	I want to obtain a list of restaurants

@Restaurant
Scenario: Retrieve a list of restaurant
	Given I have entered the outcode "se19" in the application
	When I press Enter
	Then the application shows a list of restaurant 

@Restaurant
Scenario: Retrieve an empty list
	Given I have entered the outcode "xxx" in the application
	When I press Enter
	Then the application shows an empty list of restaurant
