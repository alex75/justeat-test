﻿using System;
using System.Collections.Generic;
using JustEat.Services;
using JustEat.Services.ApiConsumers;
using JustEat.Services.Builders;
using JustEat.Services.Entities;
using NUnit.Framework;
using Moq;
using Should;


namespace JustEat.Tests.Unit_Tests
{
    [TestFixture]
    public class RestaurantServiceTest
    {
		[Test]
		public void Create_ShouldCreateAnInstance()
		{
			var service = RestaurantService.Create();
			service.ShouldNotBeNull();
		}

        [Test]
        public void ListRestaurants_Should_ReturnTheRestaurantList()
        {
            string outCode = "code 123";
            Restaurant testRestaurant = new Restaurant("Restaurant Test");
            testRestaurant.Rating = 5;
            testRestaurant.AddCuisineTypes("Type AAA", "Type CCC");


            Mock<IApiConsumer> apiConsumer = new Mock<IApiConsumer>();
            Mock<IRestaurantListBuilder> listBuilder = new Mock<IRestaurantListBuilder>();

            string jsonResult = "";
            var testRestaurants = new List<Restaurant>();
            testRestaurants.Add(testRestaurant);

            apiConsumer.Setup(c => c.ListRestaurants(It.IsAny<string>())).Returns(jsonResult);
            listBuilder.Setup(b => b.CreateList(It.IsAny<string>())).Returns(testRestaurants);

			RestaurantService service = new RestaurantService(apiConsumer.Object, listBuilder.Object);

			// Act
            var restaurants = service.ListRestaurants(outCode);

            restaurants.ShouldNotBeEmpty();
            restaurants.Count.ShouldEqual(1);
            var restaurant = restaurants[0];
            restaurant.Name.ShouldEqual(testRestaurant.Name);
            restaurant.Rating.ShouldEqual(testRestaurant.Rating);
            restaurant.CuisineTypes.ShouldNotBeEmpty();
            restaurant.CuisineTypes.ShouldEqual(testRestaurant.CuisineTypes);
        }

		[Test]
		public void ListRestaurantsAsync_Should_ReturnTheRestaurantList()
		{
			string outCode = "code 123";
			Restaurant testRestaurant = new Restaurant("Restaurant Test");
			testRestaurant.Rating = 5;
			testRestaurant.AddCuisineTypes("Type AAA", "Type CCC");


			Mock<IApiConsumer> apiConsumer = new Mock<IApiConsumer>();
			Mock<IRestaurantListBuilder> listBuilder = new Mock<IRestaurantListBuilder>();

			string jsonResult = "";
			var testRestaurants = new List<Restaurant>();
			testRestaurants.Add(testRestaurant);

			apiConsumer.Setup(c => c.ListRestaurants(It.IsAny<string>())).Returns(jsonResult);
			listBuilder.Setup(b => b.CreateList(It.IsAny<string>())).Returns(testRestaurants);

			RestaurantService service = new RestaurantService(apiConsumer.Object, listBuilder.Object);

			// Act
			var restaurants = service.ListRestaurantsAsync(outCode).Result;

			restaurants.ShouldNotBeEmpty();
			restaurants.Count.ShouldEqual(1);
			var restaurant = restaurants[0];
			restaurant.Name.ShouldEqual(testRestaurant.Name);
			restaurant.Rating.ShouldEqual(testRestaurant.Rating);
			restaurant.CuisineTypes.ShouldNotBeEmpty();
			restaurant.CuisineTypes.ShouldEqual(testRestaurant.CuisineTypes);
		}


    }
}
