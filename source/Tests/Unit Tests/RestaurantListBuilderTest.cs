﻿using JustEat.Services.Builders;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Should;

namespace JustEat.Tests.UnitTests
{
    [TestFixture]
    public class RestaurantListBuilderTest
    {
        [Test]
        public void CreateList_Should_ReturnAList()
        {
            // Arrange
            string jsonString = @"{ 
""ShortResultText"":""SE19"",
""Restaurants"":[
    {  
        ""Id"":30858, 
        ""Name"":""Toscana Italian Pizzeria"", 
        ""RatingStars"":4.98, 
        ""CuisineTypes"":[
            {""Id"":27, ""Name"":""Italian""},
            {""Id"":82, ""Name"":""Pizza""}
        ], 
    },
    {   ""Id"":26072,""Name"":""Lara's Kebab Grill"",
        ""CuisineTypes"":[ 
            { ""Id"":25, ""Name"":""Turkish""},
            { ""Id"":81,""Name"":""Kebab""}
        ]
    }
]}";

            // Act
            RestaurantListBuilder builder = new RestaurantListBuilder();
            var restaurants = builder.CreateList(jsonString);

            // Assert
            restaurants.ShouldNotBeNull();
            restaurants.ShouldNotBeEmpty();
            restaurants.Count.ShouldEqual(2);

            var restaurant = restaurants[0];
            restaurant.Name.ShouldEqual("Toscana Italian Pizzeria");
            restaurant.Rating.ShouldEqual(4.98f);
            restaurant.CuisineTypes.Count.ShouldEqual(2);
            restaurant.CuisineTypes.Any(f => f.Name == "Italian");
            restaurant.CuisineTypes.Any(f => f.Name == "Pizza");
        }
    }

}