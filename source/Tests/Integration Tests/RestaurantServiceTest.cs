﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Should;
using JustEat.Services;
using JustEat.Services.ApiConsumers;
using JustEat.Services.Builders;

namespace JustEat.Tests.IntegrationTests
{
    [TestFixture]
    public class RestaurantServiceTest :TestBase
    {

        [Test]
        public void ListRestaurants_Should_ReturnAList()
        {
            IApiConsumer apiConsumer = new ApiConsumer(apiUrl);
            IRestaurantListBuilder listBuilder = new RestaurantListBuilder();
            string outCode = "se19";

			IRestaurantService service = new RestaurantService(apiConsumer, listBuilder);

            var restaurants = service.ListRestaurants(outCode);

            restaurants.ShouldNotBeNull();
            restaurants.ShouldNotBeEmpty();
        }

        [Test]
        public void ListRestaurants_When_OutCodeIsWrong_Should_ReturnAnEmptyList()
        {
			IApiConsumer apiConsumer = new ApiConsumer(apiUrl);
            IRestaurantListBuilder listBuilder = new RestaurantListBuilder();
            string outCode = "";

			IRestaurantService service = new RestaurantService(apiConsumer, listBuilder);

            var restaurants = service.ListRestaurants(outCode);

            restaurants.ShouldNotBeNull();
            restaurants.ShouldBeEmpty();
        }

    }
}
