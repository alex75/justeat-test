﻿using JustEat.Services.ApiConsumers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Should;
using System.Configuration;

namespace JustEat.Tests.Integration_Tests
{
    [TestFixture]
    public class ApiConsumerTest: TestBase
    {      
        [Test]
        public void GetRestaurants_Should_ReturnData()
        {
            string outCode = "se19";
            ApiConsumer consumer = new ApiConsumer(apiUrl);

            var data = consumer.ListRestaurants(outCode);

            data.ShouldNotBeNull();
            data.ShouldNotBeEmpty();
        }

		[Test]
		public void GetRestaurantsAsync_Should_ReturnData()
		{
			string outCode = "se19";
			ApiConsumer consumer = new ApiConsumer(apiUrl);

			var data = consumer.ListRestaurantsAsync(outCode).Result;

			data.ShouldNotBeNull();
			data.ShouldNotBeEmpty();
		}
    }

}
