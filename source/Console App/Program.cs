﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using JustEat.Services;

namespace JustEat.ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Just Eat");

            int topNRestaurants = GetTopNRestaurants(20 /* default */);
            string outCode = GetDefaultOutCode() ?? ReadOutCode();

            try
            {

                if (outCode != null)
                {
                    Console.WriteLine(string.Format("Outcode: {0}\n", outCode));

                    IRestaurantService service = RestaurantService.Create();

                    var restaurants = service.ListRestaurants(outCode);


                    Console.WriteLine(
                        restaurants.Count == 0 ?
                        "None restaurant found!" :
                          string.Format("{0} restaurant(s) found, top {1} by rating:\n", restaurants.Count, topNRestaurants)
                        + string.Format("\n{0} {1} {2} {3}\n", "  #", "Name".PadRight(50), "Rating", "Cousine types")
                        + new string('-', 80)
                        );

                    // get top N rated Restaurants
                    restaurants = restaurants.OrderByDescending(r => r.Name).Take(topNRestaurants).ToList();
                    restaurants = restaurants.Distinct().ToList();

                    for (int i = 0; i < restaurants.Count; i++)
                    {
                        var restaurant = restaurants[i];
                        Console.WriteLine(string.Format("{0} {1} {2} {3}",
                            (i + 1).ToString().PadLeft(3),
                            restaurant.Name.PadRight(50),
                            restaurant.Rating.ToString("n1").PadRight(6),
                            string.Join(", ", restaurant.CuisineTypes.Select(c => c.Name))
                            ));
                    }
                }

            }
            catch (Exception exc)
            {
                // todo: add logging/alerting
                Console.WriteLine("Sorry... some error occurs! \nError details: {0} \n\nPlease, retry later.", exc.Message);
            }

            Console.WriteLine("\n\nPress any key to close...");
            Console.ReadKey();                
        }
        
        private static int GetTopNRestaurants(int defaultValue)
        {
            int topNRestaurants = defaultValue;
            if (ConfigurationManager.AppSettings.AllKeys.Contains("topNResults"))
                if (!int.TryParse(ConfigurationManager.AppSettings["topNResults"], out topNRestaurants))
                    topNRestaurants = defaultValue; // todo: add logging/alerting

            return topNRestaurants;			
        }

        private static string GetDefaultOutCode()
        {
            string outcode = null;
            if (ConfigurationManager.AppSettings.AllKeys.Contains("outcode"))
                outcode = ConfigurationManager.AppSettings["outcode"];

            return outcode;
        }


        private static string ReadOutCode()
        {
            string outcode = null;

            Console.Write(string.Format("Insert an Outcode:"));
            outcode = Console.ReadLine();
            return outcode;
        }

    }
}
