﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using JustEat.Services.Entities;

namespace JustEat.Services
{
    public interface IRestaurantService
    {
        IList<Restaurant> ListRestaurants(string outCode);
		Task<IList<Restaurant>> ListRestaurantsAsync(string outCode);
    }
}
