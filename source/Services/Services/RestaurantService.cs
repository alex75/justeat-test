﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JustEat.Services.Entities;
using JustEat.Services.Builders;
using JustEat.Services.ApiConsumers;
using System.Configuration;

namespace JustEat.Services
{

    public class RestaurantService : IRestaurantService
    {
        private readonly IApiConsumer apiConsumer;
        private readonly IRestaurantListBuilder restaurantListBuilder;

		public RestaurantService(IApiConsumer apiConsumer, IRestaurantListBuilder restaurantListBuilder)
        {
            this.apiConsumer = apiConsumer;
            this.restaurantListBuilder = restaurantListBuilder;
        }

		public static RestaurantService Create()
        {
            string apiUrl = ConfigurationManager.AppSettings["apiUrl"];
            IApiConsumer apiConsumer = new ApiConsumer(apiUrl);
            IRestaurantListBuilder listBuilder = new RestaurantListBuilder();
			return new RestaurantService(apiConsumer, listBuilder);
        }

        public IList<Restaurant> ListRestaurants(string outCode)
        {           
            string jsonString = apiConsumer.ListRestaurants(outCode);
            var restaurants = restaurantListBuilder.CreateList(jsonString);
            return restaurants;
        }

		public async Task<IList<Restaurant>> ListRestaurantsAsync(string outCode)
		{
			string jsonString = await apiConsumer.ListRestaurantsAsync(outCode);
			var restaurants = restaurantListBuilder.CreateList(jsonString);
			return restaurants;
		}

    }
}
