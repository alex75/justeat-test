﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustEat.Services.Entities
{
    public class Restaurant
    {
        public string Name { get; set; }
        public float Rating { get; set; }
        public ICollection<CuisineType> CuisineTypes { get; set;}

        public Restaurant(string name)
        {
            Name = name;
            CuisineTypes = new List<CuisineType>(3);
        }

        public void AddCuisineTypes(params string[] types)
        {
            foreach (var typeName in types)
				if (CuisineTypes.FirstOrDefault(t => t.Name == typeName) == null)
					CuisineTypes.Add(new CuisineType(typeName));
        }
    }
}
