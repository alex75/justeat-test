﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustEat.Services.Entities
{
    public class CuisineType
    {
        public string Name { get; private set; }

        public CuisineType(string name)
        {
            Name = name;
        }

    }
}
