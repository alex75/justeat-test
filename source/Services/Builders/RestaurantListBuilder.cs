﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JustEat.Services.Entities;
using Newtonsoft.Json.Linq;

namespace JustEat.Services.Builders
{
    public class RestaurantListBuilder : IRestaurantListBuilder
    {
        public IList<Restaurant> CreateList(string jsonString)
        {
            IList<Restaurant> list = new List<Restaurant>();

            JObject data = JObject.Parse(jsonString);
            JArray restaurants = (JArray)data["Restaurants"];

            foreach (var r in restaurants)
            {
                string name = r["Name"].Value<string>();
                JToken ratingToken = r["RatingStars"];
                float rating = ratingToken != null ? ratingToken.Value<float>() : -1;
                JToken cousineTypesToken = r["CuisineTypes"];

                Restaurant restaurant = new Restaurant(name)
                {
                    Rating = rating,
                };

                if (cousineTypesToken != null)
                    foreach (var cousine in (JArray)cousineTypesToken)
                        restaurant.AddCuisineTypes(cousine["Name"].Value<string>());

                list.Add(restaurant);
            }

            // ApiCallResult data = JsonConvert.DeserializeObject<ApiCallResult>(jsonString);

            return list;
        }
    }
}
