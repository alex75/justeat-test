﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JustEat.Services.Entities;

namespace JustEat.Services.Builders
{
    public interface IRestaurantListBuilder
    {

        IList<Restaurant> CreateList(string jsonString);
    }
}
