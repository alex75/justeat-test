﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustEat.Services.ApiConsumers
{
    public interface IApiConsumer
    {
        string ListRestaurants(string outCode);
		Task<string> ListRestaurantsAsync(string outCode);
    }
}
