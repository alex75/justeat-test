﻿using JustEat.Services.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JustEat.Services.ApiConsumers
{
    public class ApiConsumer : IApiConsumer
    {
		/* 
		https://public.je-apis.com/restaurants?q=se19
			 
		Accept-Tenant: uk
		Accept-Language: en-GB
		Authorization: Basic VGVjaFRlc3RBUEk6dXNlcjI=
		Host: public.je-apis.com
		*/


        private readonly string apiUrlBase;

        public ApiConsumer(string apiUrlBase)
        {
            this.apiUrlBase = apiUrlBase;
        }

        public string ListRestaurants(string outCode)
        {			
			string url = apiUrlBase + "restaurants?q=" + outCode;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Add("Accept-Tenant", "uk");
            client.DefaultRequestHeaders.Add("Accept-Language", "en-GB");
            client.DefaultRequestHeaders.Add("Authorization", "Basic VGVjaFRlc3RBUEk6dXNlcjI=");
			client.DefaultRequestHeaders.Add("Host", "public.je-apis.com");

			var response = client.GetAsync(url).Result;
			if (!response.IsSuccessStatusCode)
			{
				if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
					throw new Exception("API URL is wrong.");
				throw new Exception(string.Format("Fail to call API. Status: {0}. Reponse: {1}.", response.StatusCode, response.ReasonPhrase));
			}

            string result = response.Content.ReadAsStringAsync().Result;
            return result;
        }

		public async Task<string> ListRestaurantsAsync(string outCode)
		{
			string url = apiUrlBase + "restaurants?q=" + outCode;

			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(url);
			client.DefaultRequestHeaders.Add("Accept-Tenant", "uk");
			client.DefaultRequestHeaders.Add("Accept-Language", "en-GB");
			client.DefaultRequestHeaders.Add("Authorization", "Basic VGVjaFRlc3RBUEk6dXNlcjI=");
			client.DefaultRequestHeaders.Add("Host", "public.je-apis.com");

			var response = await client.GetAsync(url);
			if (!response.IsSuccessStatusCode)
			{
				if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
					throw new Exception("API URL is wrong.");
				throw new Exception(string.Format("Fail to call API. Status: {0}. Reponse: {1}.", response.StatusCode, response.ReasonPhrase));
			}

			return await response.Content.ReadAsStringAsync();			
		}


    }
}
