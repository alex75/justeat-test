# Alessandro Piccione

## How long did you spend on the coding test? 

I spent almost a full day creating the first release of the solution and then some hours improving it.
I divided the project in two parts, a service library that uses the API to retrieve data 
and a console application (not a great UI, I know) for showing the data.
I used the TDD approach and I began creating the tests (Unit and Integration tests).

I used this job to make some spikes.
First of all I used SpecFlow for the first time (and I also tried Just Eat "JustBehave" package).
I created it in Visual Studio 2015 selecting "windows universal" project types, so based on the .Net 4.6 Preview.
I encountered some problems when creating the SpecFlow tests and while making possible the use of the same solution in Visual Studio 2013 (I tested it on another PC).
I tried also to add the .Net 4.5.3/4.6 Preview on the machine with VS 2013 but it was unsuccessful and in the end I changed the solution to use the .Net 4.5.

I used a [git repository](https://bitbucket.org/alex75/justeat-test/).
I chose Bitbucket instead of GitHub because the repository can be private.

## What would you add to your solution if you had more time?
_If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add._

- Add logging to the application.
- Add checks on the API call.
- Add checks on the parsing of the API call result.
- Add some test on performance to confirm the manual parsing of JSON is better than automatic one.
- Add caching (based on outcode?)
- Name of the restaurants are not unique, in the output they should be recognizable. Showing the address seems a good choice.

## What was the most useful feature that was added to the latest version of your chosen language? 

In C# 5 "async" and "await" keywords were introduced to simplify the parallel programming.
This made possible creating *Async version of methods.

<code>
public IList<Restaurant> ListRestaurants(string outCode)
{           
	string jsonString = apiConsumer.ListRestaurants(outCode);
	var restaurants = restaurantListBuilder.CreateList(jsonString);
	return restaurants;
}

public async Task<IList<Restaurant>> ListRestaurantsAsync(string outCode)
{
	string jsonString = await apiConsumer.ListRestaurantsAsync(outCode);
	var restaurants = restaurantListBuilder.CreateList(jsonString);
	return new Task<IList<Restaurant>>(restaurants);
}
</code>

After this question I created an *Async version of the methods.

For sure I'll be a big fan of "Null-conditional Operator" and "Interpolated Strings" in C# 6.

## How would you track down a performance issue in production? Have you ever had to do this?

In my recent backend position I didn't often have problems related to the UI experience, but we have on business processes or SQL Jobs execution.
We have daily reports for the main processes results so we can see if something is "strange".
In this case we analyze the slowest queries.
My team use logging for Exceptions caused by SQL Timeout or Deadlock and we have some queries to obtain info about execution time, we use these for analysis. 
A recent problem was related to the slow rendering of AngularJS on some browsers with big and elaborated HTML tables. I optimized the generated HTML and substituted the AngularJS filters with custom functions.
Another problem was the random timeouts obtained from a client invoking one of our web services.
Analyzing the logs I found that the problem was generated every time the load balancer switched on a different server and for the first request the startup of the web application was longer than the request timeout limit of the client (60 seconds).
With the IT I checked the settings of the Application Pool about recycling rules and we removed the default recycling every 29 hours. We monitored the service to be sure there were no more memory leaks and we finally solved the problem just with this change.
Much more often I have to check if there is a slow query (sometimes slowing a web page) which one and why. I do this looking at SQL execution plan.

## How would you improve the JUST EAT APIs that you just used?

Evaluating if a version of the call that returns a light version (a reduced set of fields) is useful to reduce amount of data and time for parsing the JSON document.
If statistically the majority of the cases are those where the user doesn’t need more than the top N records it could be a good option to add pagination on the call.

## Please describe yourself using JSON.

<code>
{
	"Personal Data": {
		"First Name": "Alessandro",
		"Last Name": "Piccione",
		"Birth Date": "1975-09-08",
		"Country": "Italy"
	},
	"Professional Data": { 
		"Current Job": ".Net senior developer",
		"Specialties": ["ASP .Net", "MVC", "Web API", "SQL Server", "IIS", "RabbitMQ", "mongoDB", "Python", "Agile methodologies and ceremonies", "Mercurial", "Moq", "TeamCity"] 
		"Languages": ["it", "en"]
	},
	"Personal Info": {
		"Free Text": "I spend a lot of time on my PC testing new frameworks and features and experimenting. 
		I like writing programs (sometimes with a glass of red wine) but I'm not a technology fan, in fact I don't have a  smartphone and I prefer a desktop with a 24 IPS screen to a notebook screen.", 
		"Hobbies": [ "Programming", "Mountain biking", "3D graphic", "Sci-Fi/distopic TV series" ]
	}
}
</code>
